﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /*
         * Дополнительный пункт по желанию, попробуйте 
         * реализовать Create/Delete/Update методы в EmployeesController
         * на основе материалов ближайших уроков модуля и дополнительных материалов. 
         * Методы должны также использовать репозиторий с данными в памяти, 
         * как в базовом примере, при этом состояние списка сотрудников должно сохраняться между разными запросами.
         * */

        /// <summary>
        /// Добавить нового сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateEmployeeAsync(EmployeeCreateRequest employeeCreateRequest)
        {
            Employee employee = new Employee
            {
                FirstName = employeeCreateRequest.FirstName,
                LastName = employeeCreateRequest.LastName,
                Email = employeeCreateRequest.Email,
                Id = new Guid()
            };
            try
            {
                await _employeeRepository.CreateAsync(employee);
                return Ok(employee);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        /// <summary>
        /// Удалить запись о сотруднике по id(guid)
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteEmployeeAsync(Guid id)
        {
            try
            {
                await _employeeRepository.DeleteAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }

        /// <summary>
        /// Обновить данные о сотруднике
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateEmployeeAsync(EmployeeUpdateRequest employeeUpdateRequest)
        {
            Employee employee = new Employee
            {
                FirstName = employeeUpdateRequest.FirstName,
                LastName = employeeUpdateRequest.LastName,
                Email = employeeUpdateRequest.Email,
                Id = employeeUpdateRequest.Id
            };
                
            if (await _employeeRepository.GetByIdAsync(employeeUpdateRequest.Id) != null)
            {
                await _employeeRepository.UpdateAsync(employee);
                return Ok(employee);
            }
            else
            {
                return BadRequest();    
            }
        }
    }
}